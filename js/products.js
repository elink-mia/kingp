$(function() {
    $('.sli-osusume').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        speed: 1000,
        fade: true,
        arrows: true,
        nextArrow: '<button class="slick-next"><span class="iconify" data-icon="ic:baseline-keyboard-arrow-right" data-inline="false"></span></button>',
        prevArrow: '<button class="slick-prev"><span class="iconify" data-icon="ic:baseline-keyboard-arrow-left" data-inline="false"></span></button>'
    });
    $('.sli-osusume .ellipsis').ellipsis({
        row: 4
    });
    $('.sli-hot').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        speed: 1000,
        arrows: true,
        dots: true,
        nextArrow: '<button class="slick-next"><span class="iconify" data-icon="ic:baseline-keyboard-arrow-right" data-inline="false"></span></button>',
        prevArrow: '<button class="slick-prev"><span class="iconify" data-icon="ic:baseline-keyboard-arrow-left" data-inline="false"></span></button>',
        responsive: [{
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
    });
});