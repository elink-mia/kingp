$(function () {
    $('.sub-menu a').bind('click', function (e) {
        e.preventDefault(); // prevent hard jump, the default behavior
        var target = $(this).attr("href"); // Set the target as variable
        // perform animated scrolling by getting top-position of target-element and set it as scroll target
        $('html, body').stop().animate({
            scrollTop: $(target).offset().top
        }, 600, function () {
            location.hash = target; //attach the hash (#jumptarget) to the pageurl
        });
        return false;
    });
    $(window).scroll(function () {
        var scrollDistance = $(window).scrollTop(),
            test = $('.sub-menu').offset().top,
            logoH = $('#header .logo').height();
        // Assign active class to nav links while scolling
        $('section').each(function (i) {
            if ($(this).position().top <= scrollDistance) {
                $('.sub-menu li.active').removeClass('active');
                $('.sub-menu li').eq(i).addClass('active');
            }
        });
        // console.log(test, scrollDistance);
        // if (scrollDistance >= test) {
        //     $('.sub-menu').addClass('active').css('top',logoH);
        // } else {
        //     $('.sub-menu').removeClass('active').css('top',auto);
        // }
    }).scroll();
    $(window).scroll(function () {
        var scrollDistance = $(window).scrollTop(),
            test = $('.title-set').offset().top,
            logoH = $('#header').height();
        console.log(test, scrollDistance);
        if (scrollDistance > 251) {
            $('.sub-menu').addClass('active').css('top',logoH);
            $('#header').addClass('no-effect');
        } else {
            $('.sub-menu').removeClass('active');
            $('#header').removeClass('no-effect');
        }
    });
});