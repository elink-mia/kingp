(function($) {
    var dec_height = function() {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').innerHeight(),
            need_h = win_h - content;
        if (content < win_h) {
            $('#main').css("min-height", main_h + need_h);
        }
        $(window).resize(function() {
            if (content < win_h) {
                $('#main').css("min-height", main_h + need_h);
            }
        });
    };
    $(window).on('load', function() {
        dec_height();
    });
    var pt_h = function() {
        var logoH = $('#header .logo').height(),
            height = logoH + 50;
        $("#main.pdt").css('padding-top',height);
        $(window).resize(function() {
            $("#main.pdt").css('padding-top',height);
        });
    };
    var burgerMenu = function() {
        $('.menu').click(function() {
            $('.offcanvas-collapse').toggleClass('open');
            if ($('.offcanvas-collapse').hasClass('open')) {
                $('.burger, #header .logo').addClass('active');
                $('#header').addClass('fixed');
                $('body').addClass("no-scroll");
                $('.overlay').fadeIn();
                $('.overlay').on('click', function() {
                    // open sidebar
                    $('.offcanvas-collapse').removeClass('open');
                    $('.burger').removeClass('active');
                    $('#header').removeClass('fixed');
                    $('.overlay').fadeOut();
                });
            } else {
                $('.burger').removeClass('active');
                $('#header').removeClass('fixed');
                $('body').removeClass("no-scroll");
                $('.sub-menu').removeClass('active');
                $('.overlay').fadeOut();
            }
        });
    };
    var windowScroll = function() {
        var winScroll = $(window).scrollTop() >= 1,
            pageH = $('#page').height(),
            winH = $(window).height(),
            headerH = $('#header').height(),
            x = pageH - winH;
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 1) {
                $('#header').addClass('fixed');
            } else {
                $('#header').removeClass('fixed');
            }
        });
    };
    var goToTop = function() {
        $('.js-gotop').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');
            return false;
        });
    };
    var mob_search = function() {
        //mob版 - 商品搜尋
        $(".icon-srh").click(function(e) {
            e.preventDefault();
            $("#mob-srh").addClass("active");
            $('.overlay').fadeIn();
            $('body').addClass("no-scroll");
            $('#header .menu').css("z-index","99");
        });
        $("#srh-close, .overlay").click(function() {
            $("#mob-srh").removeClass("active");
            $('.overlay').fadeOut();
            $('body').removeClass("no-scroll");
            $('#header .menu').css("z-index","333");
        });
    };
    $(function() {
        pt_h();
        burgerMenu();
        windowScroll();
        goToTop();
        mob_search();
    });
})(jQuery)