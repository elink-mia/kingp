$(function () {
    var dec_height = function (first) {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').height(),
            need_h = win_h - content;
        if (content < win_h || typeof first != 'undefined') {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    dec_height(true);
    $(window).on('load', function () {
        footer_mb();
    });
    $(window).resize(function () {
        footer_mb();
        dec_height();
    });
    $("#header .icon-set .icon-srh").hide();
    var filterSingle = $('.filtr-container').filterizr({
        layout: 'sameWidth'
    });
    $('.grid-filter li').click(function () {
        $('.grid-filter li').removeClass('active');
        $(this).addClass('active');
        var filter = $(this).data('filter');
        filterSingle.filterizr('filter', filter);
    });
    $('.quantity').each(function () {
        var index = $(this).children('input').val() != '' ? $(this).children('input').val() : 0;
        $(this).children('input').val(index);
        $(this).children('.add').on('click', function () {
            index++;
            $(this).siblings('input').val(index);
        });
        $(this).children('.sub').on('click', function () {
            index--;
            $(this).siblings('input').val(index);
        });
    });
    // when opening the sidebar
    $('.SideBar').on('click', function () {
        $('#sidebar').addClass('active');
        $('body').addClass("no-scroll");
        $('.overlay').fadeIn();
    });
    // if dismiss or overlay was clicked
    $('.dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').fadeOut();
        $('body').removeClass("no-scroll");
    });
    $('#twzipcode, #twzipcode2').twzipcode();
});
var footer_mb = function () {
    var mob_buyH = $('#mob-buy').innerHeight(),
        winW = $(window).width();
    if (winW < 992) {
        $('#main').css('margin-bottom', mob_buyH);
    }
};