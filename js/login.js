$(function() {
    $('.sli-ad').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 2000,
        fade: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        cssEase: 'linear'
    });
    $('#twzipcode2').twzipcode();
});