$(function () {
    fixed();
    footer_mb();
    $('.SideBar, .icon-srh').on('click', function () {
        if ($('body').hasClass('no-scroll')) {
            $('body').removeClass("no-scroll");
        }
    });
    $('#twzipcode, #twzipcode2').twzipcode();
    $('#invoice-method').change(function () {
        $('.inv-').hide();
        $('.inv-' + $(this).val()).show("slow");
    }).change();
});
var footer_mb = function () {
    var mob_buyH = $('#mob-buy').innerHeight(),
        needH = mob_buyH - 45,
        winW = $(window).width();
    if (winW < 992) {
        $('#main').css('margin-bottom', needH);
    }
    $(window).resize(function () {
        if (winW < 992) {
            $('#main').css('margin-bottom', needH);
        }
    });
};
var fixed = function () {
    var total_w = $('#deli-pay #total-block').width(),
        winH = $(window).height(),
        pageH = $('#deli-pay #total-block').scrollTop(),
        headerH = $('#header').height();
    $(window).scroll(function () {
        if ($(window).width() >= 992 && headerH > pageH) {
            $('#deli-pay #total-block').addClass('fixed').width(total_w);
        }
        if ($(window).scrollTop() < headerH) {
            $('#deli-pay #total-block').removeClass('fixed');
        }
    });
    $(window).resize(function () {
        $(window).scroll(function () {
            if ($(window).width() >= 992 && headerH > pageH) {
                $('#deli-pay #total-block').addClass('fixed').width(total_w);
            }
            if ($(window).scrollTop() < headerH) {
                $('#deli-pay #total-block').removeClass('fixed');
            }
        });
    });
};