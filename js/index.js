$(function () {
  autoheight();
  AOS.init();
  scrolll();
  $('.sli-banner').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    arrows: false,
    infinite: true,
    speed: 600,
    fade: true,
    cssEase: 'linear',
    pauseOnFocus: false
  });
  $('.sli-for').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.sli-nav',
    nextArrow: '<button class="slick-next"><span class="iconify" data-icon="ion:md-arrow-dropright" data-inline="false"></span></button>',
    prevArrow: '<button class="slick-prev"><span class="iconify" data-icon="ion:md-arrow-dropleft" data-inline="false"></span></button>'
  });
  $('.sli-nav').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.sli-for',
    dots: false,
    arrows: false,
    fade: true
  });
  $('.sli-news').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    speed: 1000,
    vertical: true,
    verticalSwiping: true,
    nextArrow: '<button class="slick-next"><span class="iconify" data-icon="ion:md-arrow-dropright" data-inline="false"></span></button>',
    prevArrow: '<button class="slick-prev"><span class="iconify" data-icon="ion:md-arrow-dropleft" data-inline="false"></span></button>',
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.sli-films').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    speed: 1000,
    vertical: true,
    verticalSwiping: true,
    nextArrow: '<button class="slick-next"><span class="iconify" data-icon="ion:md-arrow-dropright" data-inline="false"></span></button>',
    prevArrow: '<button class="slick-prev"><span class="iconify" data-icon="ion:md-arrow-dropleft" data-inline="false"></span></button>',
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });
  $("#fcta .act-btn .btn").click(function () {
    var videoid = $(this).data('name');
    $(".video-container iframe").remove();
    $('<iframe width="1280" height="720" frameborder="0" allowfullscreen></iframe>')
      .attr("src", "http://www.youtube.com/embed/" + videoid + "?autoplay=1")
      .appendTo(".video-container");
  });
  $('.sli-osusume').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 1000,
    fade: true,
    arrows: true,
    nextArrow: '<button class="slick-next"><span class="iconify" data-icon="ic:baseline-keyboard-arrow-right" data-inline="false"></span></button>',
    prevArrow: '<button class="slick-prev"><span class="iconify" data-icon="ic:baseline-keyboard-arrow-left" data-inline="false"></span></button>'
  });
  ellipsisss();
});
var autoheight = function () {
  var w_h = $(window).height();
  $('.js-fullheight').height(w_h);
  $(window).resize(function () {
    $('.js-fullheight').height(w_h);
  });
};
var scrollifyyy = function () {
  $.scrollify({
    section : "section",
    interstitialSection : "#fcta, #recommend, #end",
    easing: "easeOutExpo",
    scrollSpeed: 500,
    offset : 0,
    scrollbars: true,
    standardScrollElements: "#fcta, #recommend, #end",
    setHeights: true,
    overflowScroll: true,
    updateHash: true,
    touchScroll:true,
    before:function() {},
    after:function() {},
    afterResize:function() {},
    afterRender:function() {}
  });
};
var scrolll = function () {
  var win = $(window).width();
  if (win >= 768) {
    scrollifyyy();
    // $.scrollify.disable();
    // $('section').css("height", "100%");
  }
  $(window).resize(function () {
    if (win >= 768) {
      scrollifyyy();
    }
  });
};
var ellipsisss = function () {
  var win = $(window).width();
  if (win > 375) {
    $('.ellipsis').ellipsis({
      row: 4
    });
  } else {
    $('.ellipsis').ellipsis({
      row: 2
    });
  }
  $(window).resize(function () {
    if (win > 375) {
      $('.ellipsis').ellipsis({
        row: 4
      });
    } else {
      $('.ellipsis').ellipsis({
        row: 2
      });
    }
  });
};